<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parents extends CI_Controller 
{
	function __construct()
    {
		parent::__construct();
		$this->load->library('session');
		$this->load->model('Database');
		if($this->session->userdata('role')!='parent')
		{
			redirect(base_url());
		}
    }
	
	
	public function index()
	{

		$header_data['PAGE_TITLE']="Parent's Section";
		$this->load->view("header",$header_data);
		
		$userrole = $this->session->userdata('role');
		$teacher_questions = $this->Database->getTblRecord("questions", array("question_type"=>$userrole,'status'=>'yes'));
		$child =$this->Database->getMyChild($this->session->userdata('userid'));
		$nvUsername = $this->Database->getName("users","name","user_id",$this->session->userdata('userid'));
		$masterQuestion=$this->Database->getAllRecord('questionnaires_master');
		$questions['masterquestions']=$masterQuestion;
		$nvUserID = $this->session->userdata('userid');
		
		
		
		$return['nvUsername']=$nvUsername;
		$questions['teacher_question']=$teacher_questions;
		$return['CHILD_DATA']=$child;
		$this->load->view("tpl_parents",$return);
		$this->load->view("modal",$questions);
		$this->load->view("footer");
	}
	
	public function master_parent_questions()
	{
		
		$nvUserID = $this->session->userdata('userid');
		$userrole = $this->session->userdata('role');
		$masterQuestion=$this->Database->getAllRecord('questionnaires_master');

		$nvmodelslider = "";
		if(isset($_POST['childid'])) 
		{
			$childrenname = $this->Database->getName("children","name","id",$_POST['childid']);
			$childage = $this->Database->getAge("children","id",$_POST['childid']);
			?>
			<div class="bxslider nvbxslider" id="master_question_insert">
			<?php
			$last_slide = count($masterQuestion)-1;

			foreach($masterQuestion as $tkey=>$val)
			{
				$nvQuestionID = $val->que_id;
				$nvmainAnswer = $this->Database->getRecordSubAnsID('capa_red_main_answers',"user_id",$nvUserID,"child_id",$_POST['childid'],'question_id',$nvQuestionID,"answer");
				$nvcapayescomment = $this->Database->getRecordSubAnsID('capa_red_main_answers',"user_id",$nvUserID,"child_id",$_POST['childid'],'question_id',$nvQuestionID,"parent_comment"); 
				if($nvmainAnswer == "no")
				{
					$nvnoactive = "active";
					$nvmayactive = "";
					$nvyesactive = "";
					$nvyes2active ="";
					
					$nvnoblockshow = 'style="display:block;"';
					$nvmaybeblockshow = "";
					$nvyesblockshow = "";
					$commentboxnew = 'style="display:none;"';
				}
				else if($nvmainAnswer == "maybe")
				{
					$nvnoactive = "";
					$nvmayactive = "active";
					$nvyesactive = "";
					$nvyes2active ="";
				
					$nvnoblockshow = "";
					$nvmaybeblockshow = 'style="display:block;"';
					$nvyesblockshow = "";
					
					$commentboxnew = 'style="display:none;"';
				}
				else if($nvmainAnswer == "yes")
				{
					$nvnoactive = "";
					$nvmayactive = "";
					$nvyesactive = "active";
					$nvyes2active ="";
					
					$nvnoblockshow = "";
					$nvmaybeblockshow = "";
					$nvyesblockshow = 'style="display:block;"';
					$commentboxnew = 'style="display:block;"';
				}
				else if($nvmainAnswer == "yes2")
				{
					$nvnoactive = "";
					$nvmayactive = "";
					$nvyesactive = "";
					$nvyes2active ="active";
					
					$nvnoblockshow = "";
					$nvmaybeblockshow = "";
					$nvyesblockshow = 'style="display:block;"';
					$commentboxnew = 'style="display:block;"';
				}
				else
				{
					$nvnoactive = "";
					$nvmayactive = "";
					$nvyesactive = "";
					$nvyes2active ="";
					
					$nvnoblockshow = "";
					$nvmaybeblockshow = "";
					$nvyesblockshow = "";
					$commentboxnew = 'style="display:none;"';
				}

				if($val->que_id==6)
				{
					if($childage<=12)
					{
						$nvorgquestion='If you are in a new place, does X tend to wander away from you?';
					}
					else
					{
						$nvorgquestion='Do you feel that s/he is acting too independent for his/her age?';
					}
				}
				else
				{
					$nvorgquestion = $val->question;
				}
				$nvputquestion = str_replace("X",$childrenname,$nvorgquestion);
				$nvputquestion = str_replace("[child’s first name]",$childrenname,$nvputquestion);
				$style='display:block; ';
?>				
								<!--capa-info-slide start-->
								<div class="capa-info-slide problemslide">
									<!----QUESTIONMAINSTART--->
									<div style="<?php $style; ?>" id="probe<?php echo $nvQuestionID;?>_0" class="probe<?php echo $nvQuestionID;?>_0 probe<?php echo $nvQuestionID;?>" que-id="<?php echo $nvQuestionID;?>" sub-id="0">
										<div class="capa-info-2-top">
											<div class="text">
												<p><?php echo $nvputquestion; ?></p>
											</div>
										</div>
										<input type="hidden" name="hidden_capa_userid" id="hidden_capa_userid" value="<?php echo $nvUserID; ?>"/>
										<input type="hidden" name="hidden_capa_childid" id="hidden_capa_childid" value="<?php echo $_POST['childid']; ?>"/>
										<input type="hidden" name="hidden_capa_main_ans_id" id="hidden_capa_main_ans_id" value=""/>
										<div class="capa-info-2-tab">
													<!--capa-tab start-->
													<div class="capa-tab">
														<ul class="clearfix">
															<li><a class="no mainansewerparent <?php echo $nvnoactive.' '.$nvQuestionID; ?>" data-question="<?php echo $nvQuestionID; ?>" sub-id="0" data-answer="no" href="#">No</a></li>
															<li><a class="maybe mainansewerparent <?php echo $nvmayactive.' '.$nvQuestionID; ?>" data-question="<?php echo $nvQuestionID; ?>" sub-id="0" data-answer="maybe" href="#">Unsure</a></li>
															<li><a class="yes mainansewerparent <?php echo $nvyesactive.' '.$nvQuestionID; ?>" data-question="<?php echo $nvQuestionID; ?>" sub-id="0" data-answer="yes" href="#">A Little</a></li>
															<li><a class="yes mainansewerparent <?php echo $nvyes2active.' '.$nvQuestionID; ?>" data-question="<?php echo $nvQuestionID; ?>" sub-id="0" data-answer="yes2" href="#">A Lot</a></li>
														</ul>
													</div>
													<!--capa-tab end-->

													<!--capa-tab-content start-->
													<div class="capa-tab-content">
														<div class="capa-tab-panel" <?php echo $commentboxnew; ?>  id="main_qus_<?php echo $nvQuestionID; ?>_0">
															<a class="close-btn" href="#" title="Close"></a>
															<div class="tab-box-3">
																	<div class="box-title"><h3>Comments</h3></div>
																	<div class="comment-box">
																		<textarea name="yescommentmainquestion" class="yescomment yescommentmainquestion" id="comment_main_qus_<?php echo $nvQuestionID; ?>" placeholder="please write an example of when this behaviour happens." id="yescommentmainquestion"><?php echo $nvcapayescomment; ?></textarea>
																		<div class="form-row text-center">
																			<a href="javascript:void(0)" class="submit-btn-yes submitansmaincomment" data-child="<?php echo $_POST['childid']; ?>" data-mainqus="<?php echo $nvQuestionID; ?>" title="Submit Comment"></a>
																		</div>
																	</div>
															</div>
														</div>
													</div>
													<!--capa-tab-content end-->
										</div>
									</div>
													<!----QUESTIONMAINEND--->
<?php
$que_probes=$this->Database->getSubQuestions('questionnaires_extras','que_id',$nvQuestionID);
if($que_probes!='NoValue')
{
	$sub_i=1;
	foreach($que_probes as $probe)
	{
				$style="display:none;";
				$nvorgquestion = $val->question;
				$nvputquestion = str_replace("X",$childrenname,$nvorgquestion);
				$nvputquestion = str_replace("[child’s first name]",$childrenname,$nvputquestion);
				$nvChildId= $probe->extra_id;
	?>
		<!----QUESTIONSUBEND--->
		<div style="<?php echo $style; ?>" id="probe<?php echo $nvQuestionID;?>_<?php echo $sub_i; ?>" class="probe<?php echo $nvQuestionID;?>_<?php echo $sub_i; ?> probe<?php echo $nvQuestionID;?>" que-id="<?php echo $nvQuestionID;?>" sub-que-id="<?php echo $probe->extra_id; ?>" sub-id="<?php echo $sub_i; ?>">
			<div class="capa-info-2-top">
				<div class="text">
					<p><?php echo $nvputquestion; ?></p>
				</div>
			</div>
			<input type="hidden" name="hidden_capa_userid" id="hidden_capa_userid" value="<?php echo $nvUserID; ?>"/>
			<input type="hidden" name="hidden_capa_childid" id="hidden_capa_childid" value="<?php echo $_POST['childid']; ?>"/>
			<input type="hidden" name="hidden_capa_main_ans_id" id="hidden_capa_main_ans_id" value=""/>
			<div class="capa-info-2-tab">
						<span class="subyeschild<?php echo $nvQuestionID;?>">
						<!--capa-tab start-->
						<div class="capa-tab">
							<ul class="clearfix">
								<li><a class="no subansewerparent <?php echo $nvnoactive.' '.$nvQuestionID; ?>" data-question="<?php echo $nvQuestionID; ?>" sub-que-id="<?php echo $probe->extra_id; ?>" sub-id="<?php echo $sub_i; ?>" data-answer="no" href="#">No</a></li>
								<li><a class="maybe mainansewerparent <?php echo $nvmayactive.' '.$nvQuestionID; ?>" data-question="<?php echo $nvQuestionID; ?>" sub-que-id="<?php echo $probe->extra_id; ?>" sub-id="<?php echo $sub_i; ?>" data-answer="maybe" href="#">Unsure</a></li>
								<li><a class="yes mainansewerparent <?php echo $nvyesactive.' '.$nvQuestionID; ?>" data-question="<?php echo $nvQuestionID; ?>" sub-que-id="<?php echo $probe->extra_id; ?>" sub-id="<?php echo $sub_i; ?>" data-answer="yes" href="#">A Little</a></li>
								<li><a class="yes mainansewerparent <?php echo $nvyes2active.' '.$nvQuestionID; ?>" data-question="<?php echo $nvQuestionID; ?>" sub-que-id="<?php echo $probe->extra_id; ?>" sub-id="<?php echo $sub_i; ?>" data-answer="yes2" href="#">A Lot</a></li>
							</ul>
						</div>
						<!--capa-tab end-->

						<!--capa-tab-content start-->
						<div class="capa-tab-content">
							<div class="capa-tab-panel" <?php echo $commentboxnew; ?>  id="main_qus_<?php echo $nvQuestionID; ?>_<?php echo $probe->extra_id; ?>">
								<a class="close-btn" href="#" title="Close"></a>
								<div class="tab-box-3">
										<div class="box-title"><h3>Comments</h3></div>
										<div class="comment-box">
											<textarea name="yescommentmainquestion" class="yescomment yescommentmainquestion" id="comment_main_qus_'.$nvQuestionID.'" placeholder="please write an example of when this behaviour happens." id="yescommentmainquestion"><?php echo $nvcapayescomment; ?></textarea>
											<div class="form-row text-center">
												<a href="javascript:void(0)" class="submit-btn-yes submitansmaincomment" data-child="<?php echo $_POST['childid']; ?>" data-mainqus="<?php echo $nvQuestionID; ?>" title="Submit Comment"></a>
											</div>
										</div>
								</div>
							</div>
						</div>
						<!--capa-tab-content end-->

				</div>
		</div>
		<!----QUESTIONSUBEND--->
	<?php
	$sub_i++;
	}
}
?>

<!--- Does it Worry Section Start --->
<?php
		$styleyes='none;';
		$nvuserID = $this->session->userdata('userid');	
		$postMainQuestionID = $nvQuestionID;
		$postChildID = $nvChildId;
		$questionnaires_extras=$this->Database->getSubQuestions('questionnaires_probes','que_id',$postMainQuestionID);
		$questionnaires_extras_count=$this->Database->getSubQuestionsCount('questionnaires_probes','que_id',$postMainQuestionID);
		$nvMainQuestion = $this->Database->getName("questionnaires_master","question","que_id",$postMainQuestionID);
		$nvMainQuestionHeading = $this->Database->getName("questionnaires_master","question_heading","que_id",$postMainQuestionID);
		$childage = $this->Database->getAge("children","id",$postChildID);
		$childrenname = $this->Database->getName("children","name","id",$postChildID);
		if($questionnaires_extras_count > 0)
		{
			$i=1;
			foreach($questionnaires_extras as $maybe)
			{
				$nvExtraQuestionID = $maybe->probes_id;
			
				$nvExtraAnswerCount = $this->Database->getRecordCount('capa_red_probes_answers',"user_id",$nvuserID,"child_id",$postChildID,'probes_id',$nvExtraQuestionID); 
				if($nvExtraAnswerCount > 0)
				{
					$nvExtraSubAnswerComment = $this->Database->getRecordSubAnsID('capa_red_probes_answers',"user_id",$nvuserID,"child_id",$postChildID,'probes_id',$nvExtraQuestionID,"ans_comment"); 		
					$nvcommentans = $nvExtraSubAnswerComment;
					
					$nvExtraSubAnswer = $this->Database->getRecordSubAnsID('capa_red_probes_answers',"user_id",$nvuserID,"child_id",$postChildID,'probes_id',$nvExtraQuestionID,"answer");
					
					//echo "<br>-=-=-".$i."====".$nvExtraSubAnswer;
					
					if($nvExtraSubAnswer == "no")
					{
						$nvextractiveno = "active";
						$nvextractiveyes = "";
					}
					else if($nvExtraSubAnswer == "yes")
					{
						$nvextractiveyes = "active";
						$nvextractiveno = "";
					}
					else
					{
						$nvextractiveyes = "";
						$nvextractiveno = "";
					}
					
				}
				else
				{
					$nvcommentans = "";
					$nvextractiveyes = "";
					$nvextractiveno = "";
				}
				if($nvextractiveyes == "active")
				{	$nvextraboxdisplay = 'style="display:block;"';	}
				else
				{	$nvextraboxdisplay = 'style="display:none;"';	}
				$nvputquestion = str_replace("X",$childrenname,$nvorgquestion);
				$nvputquestion = str_replace("[child’s first name]",$childrenname,$nvorgquestion);
				$nvputquestion = str_replace("[child's first name]",$childrenname,$nvorgquestion);

?>

<div id="capaworryq<?php echo $postMainQuestionID; ?>" style="display:none;">
	<div class="capa-info-2-top">
				<div class="text">
					<p><?php echo $maybe->question; ?></p>
				</div>
				<div class="capa-info-2-tab">
					<div class="capa-tab">
						<ul class="clearfix">
							<li style="float:left;"><a class="newyesans nvextracls<?php echo $maybe->probes_id; ?> no <?php echo $nvextractiveno; ?>" data-child="<?php echo $postChildID; ?>" data-ans="no" data-mainqus="<?php echo $postMainQuestionID; ?>" data-subqus="<?php echo $maybe->probes_id; ?>" href="javascript:void(0)">No</a></li>
							<!--li><a class="maybe" href="#">Maybe</a></li-->
							<li style="float:right;"><a class="newyesans nvextracls<?php echo $maybe->probes_id; ?> yes <?php echo $nvextractiveyes; ?>" data-child="<?php echo $postChildID; ?>" data-ans="yes" data-mainqus="<?php echo $postMainQuestionID; ?>" data-subqus="<?php echo $maybe->probes_id; ?>" href="javascript:void(0)">Yes</a></li>
						</ul>
					</div>
				</div>
				<!--capa-tab-content start-->
				<div class="capa-tab-content">
					<!--capa-tab-panel start-->
					<div class="capa-tab-panel" <?php echo $nvextraboxdisplay; ?> id="newcomment<?php echo $maybe->probes_id; ?>">
						<a class="close-btn" href="#" title="Close"></a>
						<div class="tab-box-3">
							<div class="box-title"><h3>Comments</h3></div>
							<div class="comment-box">
								<textarea name="yescomment" class="yescomment" placeholder="please write an example of when this behaviour happens." id="newcmt_<?php echo $maybe->probes_id; ?>"><?php echo $nvcommentans; ?></textarea>
								<div class="form-row text-center">
									<input class="submit-btn-yes cmtsubmitparentnew" id="<?php echo $maybe->probes_id; ?>" data-childid="<?php echo $postChildID; ?>" type="submit" value="Submit">
								</div>
							</div>
						</div>
					</div>
					<!--capa-tab-panel end-->
					
				</div>
				<!--capa-tab-content end-->
	</div>
</div>

<?php
				$i++;
			}
		}
?>
<!--- Does it Worry Section END --->






								</div>
								<!--capa-info-slide end-->
								<?php
			}
								?>
			</div>
			<?php
		}
	}
	
	public function parent_save_main_comment()
	{
		$nvuserID = $this->session->userdata('userid');	
		$postmainanscmtqus = $_POST['mainanscmtqus'];
		$postmainanscmtchild = $_POST['mainanscmtchild'];
		$postmainanscmtcomment = $_POST['mainanscmtcomment'];
		
		$nvAnsID = $this->Database->getRecordSubAnsID('capa_red_main_answers',"user_id",$nvuserID,"child_id",$postmainanscmtchild,'question_id',$postmainanscmtqus,"ans_id"); 
		$filed_value['parent_comment']=$postmainanscmtcomment;
		$update=$this->Database->updateRecord('capa_red_main_answers',$filed_value,'ans_id',$nvAnsID);
		echo $postmainanscmtcomment;
		
	}
	
	public function parent_yes_ans_new()
	{
		$nvuserID = $this->session->userdata('userid');	
		$postMainQuestionID = $_POST['QuestionID'];
		$postChildID = $_POST['ChildID'];
		$questionnaires_extras=$this->Database->getSubQuestions('questionnaires_probes','que_id',$postMainQuestionID);
		$questionnaires_extras_count=$this->Database->getSubQuestionsCount('questionnaires_probes','que_id',$postMainQuestionID);
		$nvMainQuestion = $this->Database->getName("questionnaires_master","question","que_id",$postMainQuestionID);
		$nvMainQuestionHeading = $this->Database->getName("questionnaires_master","question_heading","que_id",$postMainQuestionID);
		$childage = $this->Database->getAge("children","id",$postChildID);
		$childrenname = $this->Database->getName("children","name","id",$postChildID);

		if($questionnaires_extras_count > 0)
		{
			$i=1;
			$nvslides = '<div class="bxslider nvbxslidernew nvbxslidernewparent" id="master_question_insert_parent">';
			foreach($questionnaires_extras as $maybe)
			{
				$nvExtraQuestionID = $maybe->probes_id;
				
				$nvExtraAnswerCount = $this->Database->getRecordCount('capa_red_probes_answers',"user_id",$nvuserID,"child_id",$postChildID,'probes_id',$nvExtraQuestionID); 
				if($nvExtraAnswerCount > 0)
				{
					$nvExtraSubAnswerComment = $this->Database->getRecordSubAnsID('capa_red_probes_answers',"user_id",$nvuserID,"child_id",$postChildID,'probes_id',$nvExtraQuestionID,"ans_comment"); 		
					$nvcommentans = $nvExtraSubAnswerComment;
					
					$nvExtraSubAnswer = $this->Database->getRecordSubAnsID('capa_red_probes_answers',"user_id",$nvuserID,"child_id",$postChildID,'probes_id',$nvExtraQuestionID,"answer");
					
					//echo "<br>-=-=-".$i."====".$nvExtraSubAnswer;
					
					if($nvExtraSubAnswer == "no")
					{
						$nvextractiveno = "active";
						$nvextractiveyes = "";
					}
					else if($nvExtraSubAnswer == "yes")
					{
						$nvextractiveyes = "active";
						$nvextractiveno = "";
					}
					else
					{
						$nvextractiveyes = "";
						$nvextractiveno = "";
					}
					
				}
				else
				{
					$nvcommentans = "";
					$nvextractiveyes = "";
					$nvextractiveno = "";
				}
				
				//echo "<br>-=yes-=-".$nvextractiveyes;
				//$nvextractiveno;
				
				
				if($nvextractiveyes == "active")
				{
					$nvextraboxdisplay = 'style="display:block;"';
				}
				else
				{
					$nvextraboxdisplay = 'style="display:none;"';
				}
				



				if($maybe->que_id==6)
				{
					if($childage<=12)
					{
						$nvorgquestion='If you are in a new place, does X tend to wander away from you?';
					}
					else
					{
						$nvorgquestion='Do you feel that s/he is acting too independent for his/her age?';
					}
				}
				else
				{
				$nvorgquestion = $maybe->question;
				}


				$nvputquestion = str_replace("X",$childrenname,$nvorgquestion);
				$nvputquestion = str_replace("[child’s first name]",$childrenname,$nvorgquestion);
				$nvputquestion = str_replace("[child's first name]",$childrenname,$nvorgquestion);




				$nvslides.='<div class="capa-info-slide customcountslide">
                                <div class="capa-info-2-top">
                                    <div class="text">
                                    	<p>'.$nvMainQuestionHeading.'</p>
										<p>'.$nvorgquestion.'</p>
                                    </div>
                                </div>
                                <div class="capa-info-2-tab">
                                   
                                    <div class="capa-info-2-tab">
                                	<!--capa-tab start-->
                                	<div class="capa-tab">
                                    	<ul class="clearfix">
                                        	<li style="float:left;"><a class="newyesans nvextracls'.$maybe->probes_id.' no '.$nvextractiveno.'" data-child="'.$postChildID.'" data-ans="no" data-mainqus="'.$postMainQuestionID.'" data-subqus="'.$maybe->probes_id.'" href="javascript:void(0)">No</a></li>
                                            <!--li><a class="maybe" href="#">Maybe</a></li-->
                                            <li style="float:right;"><a class="newyesans nvextracls'.$maybe->probes_id.' yes '.$nvextractiveyes.'" data-child="'.$postChildID.'" data-ans="yes" data-mainqus="'.$postMainQuestionID.'" data-subqus="'.$maybe->probes_id.'" href="javascript:void(0)">Yes</a></li>
                                        </ul>
                                    </div>
                                    <!--capa-tab end-->
                                    <!--capa-tab-content start-->
                                    <div class="capa-tab-content">
                                    	<!--capa-tab-panel start-->
                                    	<div class="capa-tab-panel" '.$nvextraboxdisplay.' id="newcomment'.$maybe->probes_id.'">
											<a class="close-btn" href="#" title="Close"></a>
											<div class="tab-box-3">
												<div class="box-title"><h3>Comments</h3></div>
												<div class="comment-box">
													<textarea name="yescomment" class="yescomment" placeholder="please write an example of when this behaviour happens." id="newcmt_'.$maybe->probes_id.'">'.$nvcommentans.'</textarea>
													<div class="form-row text-center">
														<input class="submit-btn-yes cmtsubmitparentnew" id="'.$maybe->probes_id.'" data-childid="'.$postChildID.'" type="submit" value="Submit">
													</div>
												</div>
											</div>
										</div>
                                        <!--capa-tab-panel end-->
                                        
                                    </div>
                                    <!--capa-tab-content end-->
                                </div>
                                        
                                        
                                </div>
                            </div>';
					$i++;
			}
			$nvslides.= "</div>";	
		}
		else
		{
			$nvslides = "";
		}
		echo $nvslides;
	}
	
	public function parent_yes_ans_new_sub()
	{
		$nvuserID = $this->session->userdata('userid');	
		$postMainQuestionID = $_POST['QuestionID'];
		$postChildID = $_POST['ChildID'];
		$questionnaires_extras=$this->Database->getSubQuestions('questionnaires_probes','que_id',$postMainQuestionID);
		$questionnaires_extras_count=$this->Database->getSubQuestionsCount('questionnaires_probes','que_id',$postMainQuestionID);
		$nvMainQuestion = $this->Database->getName("questionnaires_master","question","que_id",$postMainQuestionID);
		$nvMainQuestionHeading = $this->Database->getName("questionnaires_master","question_heading","que_id",$postMainQuestionID);
			$childrenname = $this->Database->getName("children","name","id",$postChildID);
	
		if($questionnaires_extras_count > 0)
		{
			$i=1;
			$nvslides = '<div class="bxslider nvbxslidernew nvbxslidernew_sub" id="master_question_insert_parent_sub">';
			foreach($questionnaires_extras as $maybe)
			{
				$nvExtraQuestionID = $maybe->probes_id;
				$nvorgquestion=$maybe->question;
				$nvExtraAnswerCount = $this->Database->getRecordCount('capa_red_probes_answers',"user_id",$nvuserID,"child_id",$postChildID,'probes_id',$nvExtraQuestionID); 
				if($nvExtraAnswerCount > 0)
				{
					$nvExtraSubAnswerComment = $this->Database->getRecordSubAnsID('capa_red_probes_answers',"user_id",$nvuserID,"child_id",$postChildID,'probes_id',$nvExtraQuestionID,"ans_comment"); 		
					$nvcommentans = $nvExtraSubAnswerComment;
					
					$nvExtraSubAnswer = $this->Database->getRecordSubAnsID('capa_red_probes_answers',"user_id",$nvuserID,"child_id",$postChildID,'probes_id',$nvExtraQuestionID,"answer");
					
					//echo "<br>-=-=-".$i."====".$nvExtraSubAnswer;
					
					if($nvExtraSubAnswer == "no")
					{
						$nvextractiveno = "active";
						$nvextractiveyes = "";
					}
					else if($nvExtraSubAnswer == "yes")
					{
						$nvextractiveyes = "active";
						$nvextractiveno = "";
					}
					else
					{
						$nvextractiveyes = "";
						$nvextractiveno = "";
					}
					
				}
				else
				{
					$nvcommentans = "";
					$nvextractiveyes = "";
					$nvextractiveno = "";
				}
				
				//echo "<br>-=yes-=-".$nvextractiveyes;
				//$nvextractiveno;
				
				
				if($nvextractiveno == "active" || $nvextractiveyes == "active")
				{
					$nvextraboxdisplay = 'style="display:block;"';
				}
				else
				{
					$nvextraboxdisplay = 'style="display:none;"';
				}

				
				if($maybe->que_id==6)
				{
					if($childage<=12)
					{
						$nvorgquestion='If you are in a new place, does X tend to wander away from you?';
					}
					else
					{
						$nvorgquestion='Do you feel that s/he is acting too independent for his/her age?';
					}
				}
				else
				{
					$nvorgquestion = $maybe->question;
				}

				
				$nvputquestion = str_replace("X",$childrenname,$nvorgquestion);
				$nvputquestion = str_replace("[child’s first name]",$childrenname,$nvputquestion);


				$nvslides.='<div class="capa-info-slide customcountslide">
                                <div class="capa-info-2-top">
                                    <div class="text">
                                    	<p>'.$nvMainQuestionHeading.'</p>
										<p>'.$nvputquestion.'</p>
                                    </div>
                                </div>
                                <div class="capa-info-2-tab">
                                   
                                    <div class="capa-info-2-tab">
                                	<!--capa-tab start-->
                                	<div class="capa-tab">
                                    	<ul class="clearfix">
                                        	<li style="float:left;"><a class="newyesans nvextracls'.$maybe->probes_id.' no '.$nvextractiveno.'" data-child="'.$postChildID.'" data-ans="no" data-mainqus="'.$postMainQuestionID.'" data-subqus="'.$maybe->probes_id.'" href="javascript:void(0)">No</a></li>
                                            <!--li><a class="maybe" href="#">Maybe</a></li-->
                                            <li style="float:right;"><a class="newyesans nvextracls'.$maybe->probes_id.' yes '.$nvextractiveyes.'" data-child="'.$postChildID.'" data-ans="yes" data-mainqus="'.$postMainQuestionID.'" data-subqus="'.$maybe->probes_id.'" href="javascript:void(0)">Yes</a></li>
                                        </ul>
                                    </div>
                                    <!--capa-tab end-->
                                    <!--capa-tab-content start-->
                                    <div class="capa-tab-content">
                                    	<!--capa-tab-panel start-->
                                    	<div class="capa-tab-panel" '.$nvextraboxdisplay.' id="newcomment'.$maybe->probes_id.'">
											<a class="close-btn" href="#" title="Close"></a>
											<div class="tab-box-3">
												<div class="box-title"><h3>Comments</h3></div>
												<div class="comment-box">
													<textarea name="yescomment" class="yescomment" placeholder="please write an example of when this behaviour happens." id="newcmt_'.$maybe->probes_id.'">'.$nvcommentans.'</textarea>
													<div class="form-row text-center">
														<input class="submit-btn-yes cmtsubmitparentnew" id="'.$maybe->probes_id.'" data-childid="'.$postChildID.'" type="submit" value="Submit">
													</div>
												</div>
											</div>
										</div>
                                        <!--capa-tab-panel end-->
                                        
                                    </div>
                                    <!--capa-tab-content end-->
                                </div>
                                        
                                        
                                </div>
                            </div>';
					$i++;
			}
			$nvslides.= "</div>";	
		}
		else
		{
			$nvslides = "";
		}
		echo $nvslides;
	}
	
	public function newyesanswer()
	{
		$postsubQuestionID = $_POST['subQuestionID'];
		$postanswer = $_POST['answer'];
		$postmainqusid = $_POST['mainqusid'];
		$postchildID = $_POST['childID'];
		$nvuserID = $this->session->userdata('userid');	
		if($postanswer != "")
		{
			$nvcount = $this->Database->getRecordfourCount('capa_red_probes_answers',"user_id",$nvuserID,"main_ques_id",$postmainqusid,'probes_id',$postsubQuestionID,'child_id',$postchildID); //create user
			if($nvcount > 0)
			{
				$nvAnsID = $this->Database->getRecordSubAnsID('capa_red_probes_answers',"user_id",$nvuserID,"child_id",$postchildID,'probes_id',$postsubQuestionID,'prob_ans_id'); 		
				$filed_value['answer']=$postanswer;
				$update=$this->Database->updateRecord('capa_red_probes_answers',$filed_value,'prob_ans_id',$nvAnsID);
				if($update)
				{
					echo $nvAnsID;
				}
			}
			else
			{
				$data = array(
				   'child_id' => $postchildID,
				   'user_id' => $nvuserID,
				   'main_ques_id'=>$postmainqusid,
				   'answer'=>$postanswer,
				   'probes_id'=>$postsubQuestionID,
				);
				echo $this->Database->AddFunction('capa_red_probes_answers',$data); //create user	
			}
		}
	}
	
	public function parent_maybe_ans()
	{
		$nvuserID = $this->session->userdata('userid');	
		$postMainQuestionID = $_POST['QuestionID'];
		$postChildID = $_POST['ChildID'];
		$questionnaires_extras=$this->Database->getSubQuestions('questionnaires_extras','que_id',$postMainQuestionID);
		$questionnaires_extras_count=$this->Database->getSubQuestionsCount('questionnaires_extras','que_id',$postMainQuestionID);
		$nvMainQuestion = $this->Database->getName("questionnaires_master","question","que_id",$postMainQuestionID);
		$nvMainQuestionHeading = $this->Database->getName("questionnaires_master","question_heading","que_id",$postMainQuestionID);
		$nvExtraSubComment = "";
				

		$childrenname = $this->Database->getName("children","name","id",$postChildID);

		if($questionnaires_extras_count > 0)
		{
			$i=1;
			$nvslides = '<div class="bxslider nvbxslidernew nvbxslidernew_parent" id="master_question_insert_parent">';
			foreach($questionnaires_extras as $maybe)
			{
				$nvExtraQuestionID = $maybe->extra_id;
				$nvorgquestion=$maybe->question;
				$nvExtraAnswerCount = $this->Database->getRecordCount('capa_red_extras_answers',"user_id",$nvuserID,"child_id",$postChildID,'extra_qus_id',$nvExtraQuestionID); 
				if($nvExtraAnswerCount > 0)
				{
					$nvExtraSubAnswer = $this->Database->getRecordSubAnsID('capa_red_extras_answers',"user_id",$nvuserID,"child_id",$postChildID,'extra_qus_id',$nvExtraQuestionID,"answer"); 		
					
					$nvExtraSubComment = $this->Database->getRecordSubAnsID('capa_red_extras_answers',"user_id",$nvuserID,"child_id",$postChildID,'extra_qus_id',$nvExtraQuestionID,"extra_comment"); 		
					//$nvcommentans = $nvExtraSubAnswer;
					$nvcommentans = "";
				
					if($nvExtraSubAnswer == "no")
					{
						$nvnoactive = "active";
						$nvyesactive = "";
						$nvyes2active = "";
						$nvmaybeactive = "";
						$nvyesexample = 'style="display:none;"';
					}
					else if($nvExtraSubAnswer == "yes")
					{
						$nvyesactive = "active";
						$nvyes2active = "";
						$nvnoactive = "";
						$nvmaybeactive = "";
						$nvyesexample = 'style="display:block;"';
						
					}
					else if($nvExtraSubAnswer == "yes2")
					{
						$nvyesactive = "";
						$nvyes2active = "active";
						$nvnoactive = "";
						$nvmaybeactive = "";
						$nvyesexample = 'style="display:block;"';
						
					}
					else if($nvExtraSubAnswer == "maybe")
					{
						$nvmaybeactive = "active";
						$nvyes2active = "";
						$nvyesactive = "";
						$nvnoactive = "";
						$nvyesexample = 'style="display:none;"';
					}
					else
					{
						$nvnoactive = "";
						$nvyesactive = "";
						$nvyes2active = "";
						$nvmaybeactive = "";
						$nvyesexample = 'style="display:none;"';
					}
				}
				else
				{
					$nvcommentans = "";
					$nvnoactive = "";
					$nvyesactive = "";
					$nvyes2active = "";
					$nvmaybeactive = "";
					$nvyesexample = "";
				}

				if($maybe->que_id==6)
				{
					if($childage<=12)
					{
						$nvorgquestion='If you are in a new place, does X tend to wander away from you?';
					}
					else
					{
						$nvorgquestion='Do you feel that s/he is acting too independent for his/her age?';
					}
				}
				else
				{
					$nvorgquestion = $maybe->question;
				}
				$nvputquestion = str_replace("X",$childrenname,$nvorgquestion);
				$nvputquestion = str_replace("[child’s first name]",$childrenname,$nvputquestion);


				$nvslides.='<div class="capa-info-slide customcountslide">
                                <div class="capa-info-2-top">
                                    <div class="text">
                                    	<p>'.$nvMainQuestionHeading.'</p>
										<p>'.$nvputquestion.'</p>
                                    </div>
                                </div>
                                <div class="capa-info-2-tab">
                                   
                                    <div class="tab-box-2">
                                        <div class="question-form custom-form">
                                            <div class="capa-tab">
												<ul class="clearfix">
													<li><a class="no nvclsextramaybe '.$nvnoactive.'" data-ans="no" data-mainqus="'.$postMainQuestionID.'" data-childid="'.$postChildID.'" data-subid="'.$nvExtraQuestionID.'" href="javascript:void(0)">No</a></li>
													<li><a class="maybe nvclsextramaybe '.$nvmaybeactive.'" data-ans="maybe" data-mainqus="'.$postMainQuestionID.'" data-childid="'.$postChildID.'" data-subid="'.$nvExtraQuestionID.'" href="javascript:void(0)">Unsure</a></li>
													<li><a class="yes nvclsextramaybe '.$nvyesactive.'" data-ans="yes" data-mainqus="'.$postMainQuestionID.'" data-childid="'.$postChildID.'" data-subid="'.$nvExtraQuestionID.'" href="javascript:void(0)">A Little</a></li>
													<li><a class="yes nvclsextramaybe '.$nvyes2active.'" data-ans="yes2" data-mainqus="'.$postMainQuestionID.'" data-childid="'.$postChildID.'" data-subid="'.$nvExtraQuestionID.'" href="javascript:void(0)">A Lot</a></li>
												</ul>
											</div>';
                                            
											$nvslides.='<!--Example code start here--><div class="capa-tab-content">';
$nvslides.='<div class="capa-tab-panel"  id="may_be_qus_'.$maybe->extra_id.'" '.$nvyesexample.'>
											<a class="close-btn" href="#" title="Close"></a>
											<div class="tab-box-3">
												<div class="box-title"><h3>Comments</h3></div>
												<div class="comment-box">
													<textarea name="yescommentmainquestion" class="yescomment yescommentmainquestion" id="comment_sub_qus_'.$maybe->extra_id.'" placeholder="please write an example of when this behaviour happens.">'.$nvExtraSubComment.'</textarea>
													<div class="form-row text-center">
														<a href="javascript:void(0)" class="submit-btn-yes submitanssubcomment" data-child="'.$postChildID.'" data-mainqus="'.$postMainQuestionID.'" data-subqus="'.$maybe->extra_id.'" title="Submit Comment"></a>
													</div>
												</div>
											</div>
										</div>';
										$nvslides.='</div><!--Example code end here-->';
											
                                            
                                        $nvslides .='</div>
                                    </div>
                                        
                                        
                                </div>
                            </div>';
					$i++;
			}
			$nvslides.= "</div>";
		}
		else
		{
			$nvslides = "";
		}
		
		echo $nvslides;
	}
	
	public function parent_maybe_ans_save_new()
	{
		$nvuserID = $this->session->userdata('userid');	
		$postanswer = $_POST['answer'];
		$postsubqusid = $_POST['subqusid'];
		$postchildID = $_POST['childID'];
		$postmainQusID = $_POST['mainQusID'];
		$nvcount = $this->Database->getRecordCount('capa_red_extras_answers',"user_id",$nvuserID,"child_id",$postchildID,'extra_qus_id',$postsubqusid); //create user	
		if($nvcount > 0)
		{
			$nvAnsID = $this->Database->getRecordSubAnsID('capa_red_extras_answers',"user_id",$nvuserID,"child_id",$postchildID,'extra_qus_id',$postsubqusid,"extra_ans_id"); 
			$filed_value['answer']=$postanswer;
			$update=$this->Database->updateRecord('capa_red_extras_answers',$filed_value,'extra_ans_id',$nvAnsID);
			echo $postanswer;
			/*if($update)
			{
			}*/
		}
		else
		{
			$data = array(
			   'child_id' => $postchildID,
			   'user_id' => $nvuserID,
			   'main_ques_id'=>$postmainQusID,
			   'answer' =>$postanswer,
			   'extra_qus_id'=>$postsubqusid,
			);
			$this->Database->AddFunction('capa_red_extras_answers',$data); //create user	
			echo $postanswer;
		}
		
	}
	
	public function parent_maybe_ans_save()
	{
		$nvuserID = $this->session->userdata('userid');	
		$postsubqusid = $_POST['subqusid'];
		$postchildid = $_POST['childid'];
		$postmainquestionID = $_POST['mainquestionID'];
		$postmaybecomment = $_POST['maybecomment'];
		
		$nvcount = $this->Database->getRecordCount('capa_red_extras_answers',"user_id",$nvuserID,"child_id",$postchildid,'extra_qus_id',$postsubqusid); //create user		
		if($nvcount > 0)
		{
			$nvAnsID = $this->Database->getRecordSubAnsID('capa_red_extras_answers',"user_id",$nvuserID,"child_id",$postchildid,'extra_qus_id',$postsubqusid,"extra_ans_id"); 
			$filed_value['answer']=$postmaybecomment;
			$update=$this->Database->updateRecord('capa_red_extras_answers',$filed_value,'extra_ans_id',$nvAnsID);
			if($update)
			{
				echo $nvAnsID;
			}
					
		}
		else
		{
			$data = array(
			   'child_id' => $postchildid,
			   'user_id' => $nvuserID,
			   'main_ques_id'=>$postmainquestionID,
			   'answer' =>$postmaybecomment,
			   'extra_qus_id'=>$postsubqusid,
			);
			echo $this->Database->AddFunction('capa_red_extras_answers',$data); //create user		
		}
	}
	
	public function question_teacher()
	{
		$nvuserID = $this->session->userdata('userid');
		$userrole = $this->session->userdata('role');
		if($_POST['userrole'])
		{
			$questions = $this->Database->getTblRecord("questions", array("question_type"=>$_POST['userrole'],'status'=>'yes'));
			$return = "";
			if(!empty($questions))
			{
				
				$return .= '<input type="hidden" name="txtchildren_id" id="txtchildren_id" value="'.$_POST['childid'].'"/>';
				$return .= '<input type="hidden" name="user_role" id="user_role" value="'.$userrole.'"/>';
                $return .= '<input type="hidden" name="txtuser_id" id="txtuser_id" value="'.$nvuserID.'"/>';
				$return .='<div class="bxslider" id="question_insert">';
				$childrenname = $this->Database->getName("children","name","id",$_POST['childid']);
				$ans_first = 1;
				$ans_second = 2;
				$ans_third = 3;
				$ans_fourth = 4;
				$radioans = 1;
				foreach($questions as $key=>$val)
				{
					$nvQuestion = $val->question;
					$nvQuestionID = $val->question_id;
					
					$nvAnswer = $this->Database->getRecordSubAnsID('parent_children_answers',"user_id",$nvuserID,"child_id",$_POST['childid'],'question_id',$nvQuestionID,"answer"); 
					
					if($nvAnswer == 1)
					{
						$firstans = 'checked="checked"';
						$secondans = "";
						$thirdans = "";
						$fourthans = "";
					}
					else if($nvAnswer == 2)
					{
						$firstans = "";
						$secondans = 'checked="checked"';
						$thirdans = "";
						$fourthans = "";
					}
					else if($nvAnswer == 3)
					{
						$firstans = "";
						$secondans = "";
						$thirdans = 'checked="checked"';
						$fourthans = "";
					}
					else if($nvAnswer == 4)
					{
						$firstans = "";
						$secondans = "";
						$thirdans = "";
						$fourthans = 'checked="checked"';
					}
					else
					{
						$firstans = "";
						$secondans = "";
						$thirdans = "";
						$fourthans = "";
					}
					
					
					$return .= '<div class="rpq-info-slide">
                                <!--<div class="rpq-info-2-top">
                                	<div class="text-title">
                                    	<p>Relationship problem Questionnaire</p>
                                        <span>Parent</span>
                                    </div>
                                    <div class="text">
                                    	<p>Please tick the statement that best describes<i>'.$childrenname.'</i></p>
                                    </div>
                                </div>-->
                                <div class="rpq-info-2-question">
                                	<p>'.$nvQuestion.'</p>
                                    <div class="radio-btn">
                                        <input id="rpq-radio-'.$ans_first.'" class="selanswer_parent" data-question="'.$nvQuestionID.'" type="radio" name="answer'.$radioans.'" value="1" '.$firstans.'>
                                        <label for="rpq-radio-'.$ans_first.'">Exactly like '.$childrenname.'</label>
                                    </div>
                                    <div class="radio-btn">
                                        <input id="rpq-radio-'.$ans_second.'" class="selanswer_parent" data-question="'.$nvQuestionID.'" type="radio" name="answer'.$radioans.'" value="2" '.$secondans.'>
                                        <label for="rpq-radio-'.$ans_second.'">Like '.$childrenname.'</label>
                                    </div>
                                    <div class="radio-btn">
                                        <input id="rpq-radio-'.$ans_third.'" class="selanswer_parent" data-question="'.$nvQuestionID.'" type="radio" name="answer'.$radioans.'" value="3" '.$thirdans.'>
                                        <label for="rpq-radio-'.$ans_third.'">A bit like '.$childrenname.'</label>
                                    </div>
                                    <div class="radio-btn">
                                        <input id="rpq-radio-'.$ans_fourth.'" class="selanswer_parent" data-question="'.$nvQuestionID.'" type="radio" name="answer'.$radioans.'" value="4" '.$fourthans.'>
                                        <label for="rpq-radio-'.$ans_fourth.'">Not at all like '.$childrenname.'</label>
                                    </div>
                                </div>
                            </div>';
				$radioans++;			
				$ans_first = $ans_fourth+1;
				$ans_second = $ans_first+1;
				$ans_third = $ans_second+1;
				$ans_fourth = $ans_third+1;
				}
				$return .='</div>';
			}
			else
			{
				$return.='<div class="bxslider" id="question_insert"></div>';
			}
			echo $return;
		}
	}
	
	
	public function parent_answer()
	{
		$nvuserID = $this->session->userdata('userid');
		if(isset($_POST['answer']))
		{
			$nvAnswer = $_POST['answer'];
			$nvQuestionID = $_POST['questionID'];
			$nvChildren_id = $_POST['txtchildren_id'];
			
			$nvcount = $this->Database->getRecordCount('parent_children_answers',"user_id",$nvuserID,"child_id",$nvChildren_id,'question_id',$nvQuestionID); //create user
			if($nvcount > 0)
			{
				$nvAnsID = $this->Database->getRecordSubAnsID('parent_children_answers',"user_id",$nvuserID,"child_id",$nvChildren_id,'question_id',$nvQuestionID,"ans_id"); 
				$filed_value['answer']=$nvAnswer;
				$update=$this->Database->updateRecord('parent_children_answers',$filed_value,'ans_id',$nvAnsID);
				if($update)
				{
					echo $nvAnsID;
				}		
				
			}
			else
			{
				$data = array(
				   'answer' => $nvAnswer,
				   'question_id' => $nvQuestionID,
				   'child_id'=>$nvChildren_id,
				   'user_id'=>$nvuserID,
				);
				echo $this->Database->AddFunction('parent_children_answers',$data); //create user
			}
		}
	}
	
	public function teacher_answer()
	{
		$nvuserID = $this->session->userdata('userid');
		if(isset($_POST['answer']))
		{
			$nvAnswer = $_POST['answer'];
			$nvQuestionID = $_POST['questionID'];
			$nvChildren_id = $_POST['txtchildren_id'];
			
			$data = array(
			   'answer' => $nvAnswer,
			   'question_id' => $nvQuestionID,
			   'child_id'=>$nvChildren_id,
			   'user_id'=>$nvuserID,
			);
			echo $this->Database->AddFunction('parent_children_answers',$data); //create user
		}
	}
	
	public function parent_main_ans()
	{
		$nvuserID = $this->session->userdata('userid');
		$nvChildID = $_POST['ChildID'];
		$nvQuestionID = $_POST['QuestionID'];
		$nvpostAns = $_POST['MainAns'];
		$nvcount = $this->Database->getRecordCount('capa_red_main_answers',"user_id",$nvuserID,"child_id",$nvChildID,'question_id',$nvQuestionID); //create user
		if($nvcount > 0)
		{
			$nvAnsID = $this->Database->getRecordAnsID('capa_red_main_answers',"user_id",$nvuserID,"child_id",$nvChildID,'question_id',$nvQuestionID); 		
			
			$filed_value['answer']=$nvpostAns;
			$update=$this->Database->updateRecord('capa_red_main_answers',$filed_value,'ans_id',$nvAnsID);
			if($update)
			{
				echo $nvAnsID;
			}
		}
		else
		{
			if(isset($_POST['MainAns']))
			{
				$data = array(
				   'child_id' => $nvChildID,
				   'user_id' => $nvuserID,
				   'question_id'=>$_POST['QuestionID'],
				   'answer'=>$_POST['MainAns'],
				);
				echo $this->Database->AddFunction('capa_red_main_answers',$data); //create user	
			}
		}
		
	}	
	
	// prob ans start
	public function parent_prob_ans()
	{
		$postMainqustionID = $_POST['mainqustionid'];
		$postProbID = $_POST['probid'];
		$postAnswer = $_POST['answer'];
		$nvChildID = $_POST['hidden_capa_childid'];
		$nvuserID = $this->session->userdata('userid');
		
		$nvcount = $this->Database->getRecordCount('capa_red_probes_answers',"user_id",$nvuserID,"child_id",$nvChildID,'probes_id',$postProbID); //create user
		if($nvcount > 0)
		{
			$nvAnsID = $this->Database->getRecordSubAnsID('capa_red_probes_answers',"user_id",$nvuserID,"child_id",$nvChildID,'probes_id',$postProbID,"prob_ans_id"); 		
			$filed_value['answer']=$postAnswer;
			$update=$this->Database->updateRecord('capa_red_probes_answers',$filed_value,'prob_ans_id',$nvAnsID);
			if($update)
			{
				echo $nvAnsID;
			}
		}
		else
		{
			if(isset($_POST['probid']))
			{
				$data = array(
				   'child_id' => $nvChildID,
				   'user_id' => $nvuserID,
				   'main_ques_id'=>$postMainqustionID,
				   'answer'=>$postAnswer,
				   'probes_id'=>$postProbID,
				);	
				echo $this->Database->AddFunction('capa_red_probes_answers',$data); //create user	
			}
		}
	}
	// prob ans end
	
	
	//teacher_extra_ans ans start here
	public function parent_extra_ans()
	{
		$nvChildID = $_POST['hidden_capa_childid'];
		$nvuserID = $this->session->userdata('userid');
		$nvCountpost = count($_POST)-1;
		$FinalCountpost = ($nvCountpost)/3;
		for($i=1;$i<=$FinalCountpost;$i++)
		{
			$nvans = $_POST["maybe_ans_".$i];
			$nvSubQus = $_POST['hidden_maybe_que_'.$i];
			$nvMainQus = $_POST['hidden_main_question_'.$i];
			$nvcount = $this->Database->getRecordCount('capa_red_extras_answers',"user_id",$nvuserID,"child_id",$nvChildID,'extra_qus_id',$nvSubQus); //create user			
			if($nvcount > 0)
			{
				$nvAnsID = $this->Database->getRecordSubAnsID('capa_red_extras_answers',"user_id",$nvuserID,"child_id",$nvChildID,'extra_qus_id',$nvSubQus,"extra_ans_id"); 
				$filed_value['answer']=$nvans;
				$update=$this->Database->updateRecord('capa_red_extras_answers',$filed_value,'extra_ans_id',$nvAnsID);
				if($update)
				{
					echo $nvAnsID;
				}
						
			}
			else
			{
				$data = array(
				   'child_id' => $nvChildID,
				   'user_id' => $nvuserID,
				   'main_ques_id'=>$nvMainQus,
				   'answer' =>$nvans,
				   'extra_qus_id'=>$nvSubQus,
				);
				echo $this->Database->AddFunction('capa_red_extras_answers',$data); //create user		
			}
		}
	}
	
	
	
	public function parent_yes_ans_new_comment()
	{
		$postChildID = $_POST['subquschildID'];
		$postMainQusID = $_POST['submainqusID'];
		$postSubQusID = $_POST['subsubqusID'];
		$postcomment = $_POST['comment'];
		$nvuserID = $this->session->userdata('userid');
		$nvcount = $this->Database->getRecordCount('capa_red_extras_answers',"user_id",$nvuserID,"child_id",$postChildID,'extra_qus_id',$postSubQusID);
		if($nvcount > 0)
		{
			$nvAnsID = $this->Database->getRecordSubAnsID('capa_red_extras_answers',"user_id",$nvuserID,"child_id",$postChildID,'extra_qus_id',$postSubQusID,"extra_ans_id"); 
			$filed_value['extra_comment']=$postcomment;
			$update=$this->Database->updateRecord('capa_red_extras_answers',$filed_value,'extra_ans_id',$nvAnsID);
			if($update)
			{
				echo $nvAnsID;
			}else
			{
				echo $nvAnsID;
			}
		}
		else
		{
			$data = array(
			   'child_id' => $postChildID,
			   'user_id' => $nvuserID,
			   'main_ques_id'=>$postMainQusID,
			   'extra_qus_id'=>$postSubQusID,
			   'extra_comment' =>$postcomment,
			);
			echo $this->Database->AddFunction('capa_red_extras_answers',$data); //create user		
		}
	}
	
	
	//teacher_extra_ans ans End here	
	public function parent_yes_comment()
	{
		$nvChildID = $_POST['hidden_capa_childid'];
		$nvuserID = $this->session->userdata('userid');
		$postComment = $_POST['comment'];
		$postMainquestionID = $_POST['mainquestionID'];
		$nvcount = $this->Database->getRecordCount('capa_red_yes_answers',"user_id",$nvuserID,"child_id",$nvChildID,'main_ques_id',$postMainquestionID);
		if($nvcount)
		{
			$nvAnsID = $this->Database->getRecordSubAnsID('capa_red_yes_answers',"user_id",$nvuserID,"child_id",$nvChildID,'main_ques_id',$postMainquestionID,"yes_ans_id"); 
			$filed_value['comment']=$postComment;
			$update=$this->Database->updateRecord('capa_red_yes_answers',$filed_value,'yes_ans_id',$nvAnsID);
			if($update)
			{
				echo $nvAnsID;
			}
		}
		else
		{
			$data = array(
				   'child_id' => $nvChildID,
				   'user_id' => $nvuserID,
				   'main_ques_id'=>$postMainquestionID,
				   'comment' =>$postComment,
				);
				echo $this->Database->AddFunction('capa_red_yes_answers',$data); //create user		
		}
					
	}
	
	public function parent_yes_new_comment()
	{
		$postsubquestionID = $_POST['subquestionID'];
		$postcomment = $_POST['comment'];
		$postchildID = $_POST['childID'];
		$nvuserID = $this->session->userdata('userid');
		$nvcount = $this->Database->getRecordCount('capa_red_probes_answers',"user_id",$nvuserID,"child_id",$postchildID,'probes_id',$postsubquestionID);
		if($nvcount > 0)
		{
			$nvAnsID = $this->Database->getRecordSubAnsID('capa_red_probes_answers',"user_id",$nvuserID,"child_id",$postchildID,'probes_id',$postsubquestionID,"prob_ans_id"); 
			$filed_value['ans_comment']=$postcomment;
			$update=$this->Database->updateRecord('capa_red_probes_answers',$filed_value,'prob_ans_id',$nvAnsID);
			if($update)
			{
				$nvAnsID;
			}
		}
		else
		{
				$data = array(
				   'child_id' => $postchildID,
				   'user_id' => $nvuserID,
				   'probes_id'=>$postsubquestionID,
				   'ans_comment' =>$postcomment,
				);
				$this->Database->AddFunction('capa_red_probes_answers',$data); //create user		
		}
	}
	
	
}